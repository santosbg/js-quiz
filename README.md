# Quiz game

## Rules
The question are opened from the row with the easiest questions to the row with the hardest questions. 
Each team member choose a qustion from the current row. If he answer correctly he receives the corresponding
points. If not some of the other team members can answer and get the points.

The questions are mostly from JavaScript Core.

The game is set for 5 players(there are 5 questions for each level). To set it up for more people just add more questions in each level in the collection of questions.

### To start
- run it in the browser

### To open a question
- click on a container

### To open a code question (the last row)
- click on a container and wait for the animation
- click again on the same container

![alt text](https://i.imgur.com/hxL4vHS.png)

P.S. - I ask additional questions about the topic if the member is not absolutely sure about his answer in order
to give him more points.  
P.S. - There is planty of space for more functionality, re-design and any other modifications.