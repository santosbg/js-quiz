export const questions = [
  // Level one - 1 point for successful answer
  {
    question: 'What is npm?',
    points: 1,
    type: 'text'
  },
  {
    question: 'What is JavaScript?',
    points: 1,
    type: 'text'
  },
  {
    question: 'What is Node.js?',
    points: 1,
    type: 'text'
  },
  {
    question: 'What is HTML?',
    points: 1,
    type: 'text'
  },
  {
    question: 'What is CSS?',
    points: 1,
    type: 'text'
  },

  // Level two - 2 points for successful answer
  {
    question: 'Data types in JavaScript?',
    points: 2,
    type: 'text'
  },
  {
    question: 'Function types in JavaScript?',
    points: 2,
    type: 'text'
  },
  {
    question: 'Variable types in JavaScript?',
    points: 2,
    type: 'text'
  },
  {
    question: 'What is array?',
    points: 2,
    type: 'text'
  },
  {
    question: 'What is object?',
    points: 2,
    type: 'text'
  },

  // Level tree - 3 points for successful answer
  {
    question: 'What is closure?',
    points: 3,
    type: 'text'
  },
  {
    question: 'What is hoisting?',
    points: 3,
    type: 'text'
  },
  {
    question: 'What is scope and are the types of the scope?',
    points: 3,
    type: 'text'
  },
  {
    question: 'What are the modules? How to achive modularity?',
    points: 3,
    type: 'text'
  },
  {
    question: 'What is the browser?',
    points: 3,
    type: 'text'
  },

  // Level four - 4 points for successful answer
  {
    image: './assets/1.png',
    points: 4,
    type: 'image'
  },
  {
    image: 'assets/2.png',
    points: 4,
    type: 'image'
  },
  {
    image: 'assets/3.png',
    points: 4,
    type: 'image'
  },
  {
    image: 'assets/4.png',
    points: 4,
    type: 'image'
  },
  {
    image: 'assets/5.png',
    points: 4,
    type: 'image'
  }
];

//=====Code for output questions======

// if (true) {
//   let b = 5;
// }
// console.log(b)

// for(var i = 0; i < 5; i++) {}
// console.log(i)

// console.log(i);
// var i = 5;

// console.log(i);
// const i = 5;

// fn(5);
// function fn(num) {
//   console.log(num);
// }