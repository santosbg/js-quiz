import { questions } from './qustions.js';
import { getQuestionHTML, getParticipantHTML, getModalHTML } from './utils.js';

(() => {
  const questionsCollection = questions;
  const html = $('html')
  const grid = $('#questions-grid');
  const addPaticipantBtn = $('#add-participant-btn');
  const addParticipantInput = $('#add-participant-input');
  const participantsList = $('#participants-list');

  // On click add new participant
  addPaticipantBtn.on('click', () => {
    // Get paticipant name
    const newParticipant = addParticipantInput.val();
    // Get participant container html
    const participantHTML = getParticipantHTML(newParticipant);

    // Add the participant container to the html
    participantsList.append(participantHTML);
    // Clear the input container
    addParticipantInput.val('');
  })

  questionsCollection.forEach((question, index) => {
    // Get question container html
    const questionHTML = getQuestionHTML(question, index);

    // Add new question container
    grid.append(questionHTML);

    if (question.type === 'image') {
      // Get modal html
      const modalHTML = getModalHTML(question, index);

      // Add modal to the html
      html.append(modalHTML);

      // On click display modal with the code question image
      $(`#back${-index}`).on('click', () => {
        $(`#modal-${index}`).css('display', 'flex');
      });

      // Close modal
      $(`#close-btn-${index}`).on('click', () => {
        $(`#modal-${index}`).css('display', 'none');
      });
    }

    // Flip the cards on click
    $('.flipper-container').unbind().on('click', function () {
      const flipper = $(this).children('.flipper');
      flipper.css('transform', 'rotateY(180deg)');
    });
  })
})();
