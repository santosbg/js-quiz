const getQuestionHTML = (questionData, id) => {
  if (questionData.type === 'text') {
    return `
      <div class="flipper-container" id="${id}">
        <div class="flipper">
          <div class="front">
            ${questionData.points}
          </div>
          <div class="back">
            ${questionData.question}
          </div>
        </div>
      </div>
    `;
  } else {
    return `
      <div class="flipper-container" id="${id}">
        <div class="flipper">
          <div class="front">
            ${questionData.points}
          </div>
          <div class="back" id="back-${id}">
            What is the output?
          </div>
        </div>
      </div>
    `;
  }
};

const getParticipantHTML = (participantName) => {
  return `
    <li class="participant">
      <span class="participant-name">${participantName}</span>
      <input type="text" class="participant-score" placeholder="0">
    </li>
  `;
};

const getModalHTML = (questionData, id) => {
  return `
    <div class="code" id="modal-${id}">
      <h2 class="code-title">
        What is the output?
        <span class="code-close-btn" id="close-btn-${id}">X</span>
      </h2>
      <img src="${questionData.image}" />
    </div>
  `;
};

export {
  getQuestionHTML,
  getParticipantHTML,
  getModalHTML
}